use axum::response::Html;
use axum::{extract::Path, handler::get, response::IntoResponse, Router};
use googapis::{
    google::assistant::embedded::v1alpha2::{
        assist_config, assist_request, audio_out_config::Encoding,
        embedded_assistant_client::EmbeddedAssistantClient, AssistConfig, AssistRequest,
        AudioOutConfig, DeviceConfig,
    },
    CERTIFICATES,
};
use oauth2::reqwest::async_http_client;
use oauth2::{AuthUrl, ClientId, ClientSecret, RefreshToken, TokenResponse, TokenUrl};
use std::net::SocketAddr;
use tonic::{
    metadata::MetadataValue,
    transport::{Certificate, Channel, ClientTlsConfig},
    Request,
};

#[derive(Debug, Clone)]
struct Config {
    client_id: String,
    client_secret: String,
    refresh_token: String,
    allowed_commands: Vec<String>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    run().await
}

fn get_config() -> Config {
    // FIXME: look into how axum passes context down into request handlers.

    let client_id =
        std::env::var("GOOGLE_CLIENT_ID").expect("GOOGLE_CLIENT_ID must be set in env or .env");
    let client_secret = std::env::var("GOOGLE_CLIENT_SECRET")
        .expect("GOOGLE_CLIENT_SECRET must be set in env or .env");
    let refresh_token =
        std::env::var("REFRESH_TOKEN").expect("REFRESH_TOKEN must be set in env or .env");

    let allowed_commands: Vec<String> = std::env::var("ALLOWED_COMMANDS")
        .expect("ALLOWED_COMMANDS must be set in env or .env")
        .split(',')
        .map(|s| s.to_string())
        .collect();
    Config {
        client_id,
        client_secret,
        refresh_token,
        allowed_commands,
    }
}

async fn run() -> Result<(), Box<dyn std::error::Error>> {
    dotenv::dotenv().ok();

    // build our application with a route
    let app = Router::new()
        .route("/", get(root_handler))
        .route("/:command", get(command_handler).post(command_handler));

    // run it
    let addr = SocketAddr::from(([0, 0, 0, 0], 8080));
    println!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}

fn command_link(url: &str) -> String {
    format!(r#"<a href="/{url}">/{url}</a>"#, url = url)
}
fn li(el: &str) -> String {
    format!("<li>{}</li>", el)
}

fn page(message: &str) -> Html<String> {
    let config = get_config();

    Html(format!(
        r#"<html>
            <body>
                <p>{message}</p>
                <p>Available commands are: </p>
                <ul>{commands}</ul>
            </body>
        </html>"#,
        message = message,
        commands = config
            .allowed_commands
            .iter()
            .map(|c| li(&command_link(c)))
            .collect::<Vec<_>>()
            .join("\n")
    ))
}

async fn root_handler() -> Html<String> {
    page("Welcome to qbutton-server.")
}

async fn command_handler(Path(command): Path<String>) -> impl IntoResponse {
    // FIXME get axum to urldecode this for me?
    let command = dbg!(command.replace("%20", " "));

    let config = get_config();

    if !config.allowed_commands.contains(&command) {
        // FIXME: return something other than 200?
        return page(&format!(
            "command {:?} is not in ALLOWED_COMMANDS {:?}",
            command, config.allowed_commands
        ));
    }

    // FIXME: work out how to do error propagation properly in axum
    let token = get_token(&config).await.unwrap();
    make_request(token, command).await.unwrap();

    page("Done")
}

async fn get_token(config: &Config) -> Result<String, Box<dyn std::error::Error>> {
    let client = oauth2::basic::BasicClient::new(
        ClientId::new(config.client_id.to_string()),
        Some(ClientSecret::new(config.client_secret.to_string())),
        AuthUrl::new("https://oauth2.googleapis.com/auth".to_string())?,
        Some(TokenUrl::new(
            "https://oauth2.googleapis.com/token".to_string(),
        )?),
    );

    let token_response = client
        .exchange_refresh_token(&RefreshToken::new(config.refresh_token.to_string()))
        .request_async(async_http_client)
        .await?;

    Ok(token_response.access_token().secret().clone())
}

async fn make_request(bearer: String, command: String) -> Result<(), Box<dyn std::error::Error>> {
    let tls_config = ClientTlsConfig::new()
        .ca_certificate(Certificate::from_pem(CERTIFICATES))
        .domain_name("embeddedassistant.googleapis.com");

    let channel = Channel::from_static("https://embeddedassistant.googleapis.com")
        .tls_config(tls_config)?
        .connect()
        .await?;

    let mut service =
        EmbeddedAssistantClient::with_interceptor(channel, move |mut req: Request<()>| {
            let meta = MetadataValue::from_str(&format!("Bearer {}", bearer)).unwrap();
            req.metadata_mut().insert("authorization", meta);
            Ok(req)
        });

    let config = AssistConfig {
        r#type: Some(assist_config::Type::TextQuery(command)),
        // ..Default::Default() maybe?
        audio_out_config: Some(AudioOutConfig {
            encoding: Encoding::Mp3.into(),
            sample_rate_hertz: 16000,
            volume_percentage: 50,
        }),
        device_config: Some(DeviceConfig {
            device_id: "my_device_id".to_owned(),
            device_model_id: "qhome-887f8-qhome-button-ty2jrt".to_owned(),
        }),
        debug_config: None,
        dialog_state_in: None,
        screen_out_config: None,
    };
    let request = AssistRequest {
        r#type: Some(assist_request::Type::Config(config)),
    };
    let request = futures::stream::once(async move { request });
    let response = service.assist(request).await?;

    println!("RESPONSE={:?}", response);
    if let Some(msg) = response.into_inner().message().await? {
        eprintln!("MESSAGE={:?}", msg);
    }

    Ok(())
}
